package com.config;

public class ServicesConfiguration{

    // Base URI
    public static final String URI = "https://petstore.swagger.io/v2";

    // Endpoints
    public static final String PETS = "/pet";
    public static final String ORDER = "/store/order";
    public static final String INV = "/store/inventory";
    public static final String USER = "/user";

}