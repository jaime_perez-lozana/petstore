package com.gherkinDefinitions;

import com.influxdb.InfluxDBIntegration;
import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.serenitySteps.PetStoreSteps;

import java.awt.*;
import java.util.Calendar;

public class PetStoreDefinitions {

    /**
     * Method executed before each scenario to start measuring execution times
     *
     * @param scenario Scenario object to check if the scenario contains the tag to write on InfluxDB
     */
    @Before
    public void startInfluxdb(Scenario scenario) {
        if (scenario.getSourceTagNames().contains("@influxdb"))
            Serenity.setSessionVariable("startTime").to(Calendar.getInstance());
    }

    @Steps
    private PetStoreSteps petStoreSteps;

    @When("^I request to (get|update|delete) a pet by ID \"([^\"]*)\"$")
    public void iRequestOperPetByID(String operation, String id) {
        switch (operation.toLowerCase()) {
            case "get":
                petStoreSteps.getPetById(id);
                break;
            case "update":
                petStoreSteps.updatePetById(id);
                break;
            case "delete":
                petStoreSteps.deletePetById(id);
                break;
            default:
                break;

        }
    }

    @When("^I request to (get|delete) an order by ID \"([^\"]*)\"$")
    public void iRequestOperOrderByID(String operation, String id) {
        switch (operation.toLowerCase()) {
            case "get":
                petStoreSteps.getOrderById(id);
                break;
            case "delete":
                petStoreSteps.deleteOrderById(id);
                break;
            default:
                break;

        }
    }

    @When("^I request to (get|delete) a user by ID \"([^\"]*)\"$")
    public void iRequestOperUserByID(String operation, String username) {
        switch (operation.toLowerCase()) {
            case "get":
                petStoreSteps.getUserById(username);
                break;
            case "delete":
                petStoreSteps.deleteUserById(username);
                break;
            default:
                break;

        }
    }

    @When("^I request to update a user by this JSON \"([^\"]*)\" and username \"([^\"]*)\"$")
    public void iRequestUpdateUserByID(String file, String username) {
        petStoreSteps.updateUserById(file, username);
    }

    @When("^I request to login with a user by username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void iRequestLoginUserByID(String username, String password) {
        petStoreSteps.loginUser(username, password);
    }

    @Then("I request to logout the user")
    public void iRequestLogoutUserByID() {
        petStoreSteps.logoutUser();
    }

    @When("^I request to create (a pet|a user) with this JSON \"([^\"]*)\"$")
    public void iRequestOperToCreateWithJSON(String mode, String file) {
        switch (mode.toLowerCase()) {
            case "a pet":
                petStoreSteps.createPetWithJSON(file);
                break;
            case "a user":
                petStoreSteps.createUserWithJSON(file);
                break;
            default:
                break;
        }
    }

    @When("^I request to create an order$")
    public void iRequestToCreateAnOrderWithJSONInfo() {
        petStoreSteps.createOrderWithJSONInfo();
    }

    @And("^the following json info is sent$")
    public void iSendJSONInfoToOrder(String jsonInfo) {
        petStoreSteps.iSendJSONInfoToOrder(jsonInfo);

    }

    @When("^I request to create a user by array with this JSON \"([^\"]*)\"$")
    public void iRequestToCreateAUserByArray(String file) {
        petStoreSteps.createUserByArray(file);
    }

    @When("I request to check the inventory")
    public void iRequestToCheckInventory() {
        petStoreSteps.getInventory();
    }

    @Then("I should get (.*) status code")
    public void iShouldGetStatusCode(int expectedStatusCode) {
        petStoreSteps.verifyStatusCode(expectedStatusCode);
    }

    @And("^The value for the \"([^\"]*)\" after (get|post|update|delete) operation should be \"([^\"]*)\"$")
    public void theValueForTheAfterGetOperationShouldBe(String key, String operation, String expectedValue) {
        Response res = Serenity.sessionVariableCalled("response");
        petStoreSteps.verifyValueFromKey(res, operation, key, expectedValue);
    }

    /**
     * Method executed after each scenario to write execution times on InfluxDB
     *
     * @param scenario Scenario object to write different attributes in DB
     */
    @After
    public void measureScenario(Scenario scenario) throws AWTException {

        if (scenario.getSourceTagNames().contains("@influxdb")) {
            InfluxDBIntegration bd = InfluxDBIntegration.getInstance();
            Calendar startTime = Serenity.sessionVariableCalled("startTime");
            Serenity.setSessionVariable("endTime").to(Calendar.getInstance());
            Calendar endTime = Serenity.sessionVariableCalled("endTime");
            bd.writeInfluxDB(scenario, startTime, endTime);
        }

    }
}
