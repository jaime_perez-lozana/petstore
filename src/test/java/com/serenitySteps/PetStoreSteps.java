package com.serenitySteps;

import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.Serenity;
import io.restassured.response.Response;

import static net.serenitybdd.rest.SerenityRest.rest;
import com.config.ServicesConfiguration;
import com.support.ServicesSupport;

import java.io.*;
import java.nio.charset.Charset;
import java.util.stream.Collectors;

import net.thucydides.core.annotations.Step;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.junit.Assert;

public class PetStoreSteps{

    private ServicesSupport servicesSupport = new ServicesSupport();
    private RequestSpecification spec = rest().baseUri(ServicesConfiguration.URI).contentType(ContentType.JSON).when();
    private RequestSpecification spec2 = rest().baseUri(ServicesConfiguration.URI).contentType(ContentType.XML).when();

    private String endpoint_pet = ServicesConfiguration.PETS;
    private String endpoint_order = ServicesConfiguration.ORDER;
    private String endpoint_user = ServicesConfiguration.USER;
    private String endpoint_inventory = ServicesConfiguration.INV;

    /**
     * Performs a GET operation with an ID provided by parameter from the scenario
     * @param id The ID of a pet
     * */
    @Step
    public void getPetById(String id) {

        String endpoint = endpoint_pet + "/" + id;
        Response response = servicesSupport.executeRequest(spec,"GET", endpoint);
        Serenity.setSessionVariable("response").to(response);
    }

    /**
     * Performs a GET operation with an ID provided by parameter from the scenario
     * @param id The ID of an order
     * */
    @Step
    public void getOrderById(String id) {

        String endpoint = endpoint_order + "/" + id;
        Response response = servicesSupport.executeRequest(spec,"GET", endpoint);
        Serenity.setSessionVariable("response").to(response);
    }

    /**
     * Performs a GET operation with an ID provided by parameter from the scenario
     * @param username The username of a user
     * */
    @Step
    public void getUserById(String username) {

        String endpoint = endpoint_user + "/" + username;
        Response response = servicesSupport.executeRequest(spec,"GET", endpoint);
        Serenity.setSessionVariable("response").to(response);
    }

    /**
     * Performs a POST operation that will create a new pet
     * */
    /*@Step
    public void createPet(String id){

        try {
            InputStream is = this.getClass().getResourceAsStream("/requests/create_pet_"+id+".json");
            JSONObject body = servicesSupport.jsonInputStreamToJsonObject(is);
            spec = spec.body(body.toMap());
            Response response = servicesSupport.executeRequest(spec,"POST", endpoint_pet);
            Serenity.setSessionVariable("response").to(response);
        }
        catch(Exception e){
            e.getMessage();
        }
    }*/

    @Step
    public void createPetWithJSON(String file){

        try {
            InputStream is = this.getClass().getResourceAsStream("/requests/"+file);
            JSONObject body = servicesSupport.jsonInputStreamToJsonObject(is);
            spec = spec.body(body.toMap());
            Response response = servicesSupport.executeRequest(spec,"POST", endpoint_pet);
            Serenity.setSessionVariable("response").to(response);
        }
        catch(Exception e){
            e.getMessage();
        }
    }

    /**
     * It does nothing for the moment...
     * */
    @Step
    public void createOrderWithJSONInfo() {
        System.out.println("I'm gonna create an order...");
    }

    /**
     * Performs a POST operation that will create a new order
     * */
    @Step
    public void iSendJSONInfoToOrder(String jsonInfo){
        //System.out.println("I should receive JSON info to create an order...");
        //System.out.println(jsonInfo);
        try {
            spec = spec.body(jsonInfo);
            Response response = servicesSupport.executeRequest(spec,"POST", endpoint_order);
            Serenity.setSessionVariable("response").to(response);
        }
        catch(Exception e){
            e.getMessage();
        }
    }

    /**
     * Performs a POST operation that will create a new user
     * */
    @Step
    public void createUserWithJSON(String file){

        try {
            InputStream is = this.getClass().getResourceAsStream("/requests/"+file);
            JSONObject body = servicesSupport.jsonInputStreamToJsonObject(is);
            spec = spec.body(body.toMap());
            Response response = servicesSupport.executeRequest(spec,"POST", endpoint_user);
            Serenity.setSessionVariable("response").to(response);
        }
        catch(Exception e){
            e.getMessage();
        }
    }

    /**
     * Performs a POST operation that will create a new user but instead with the simple json file, with an array in it.
     * */
    @Step
    public void createUserByArray(String file){

        try {
            InputStream is = this.getClass().getResourceAsStream("/requests/"+file);
            JSONArray body = servicesSupport.jsonInputStreamToJsonArray(is);
            spec = spec.body(body.toList());
            String endpoint = endpoint_user + "/createWithArray";
            Response response = servicesSupport.executeRequest(spec,"POST", endpoint);
            Serenity.setSessionVariable("response").to(response);
        }
        catch(Exception e){
            e.getMessage();
        }
    }

    /**
     * Performs a PUT operation with an ID from the scenario as a parameter
     * @param id The ID of a pet
     * */
    @Step
    public void updatePetById(String id) {

        try {
            InputStream is = this.getClass().getResourceAsStream("/requests/update_pet_"+id+".json");
            JSONObject body = servicesSupport.jsonInputStreamToJsonObject(is);
            spec = spec.body(body.toMap());
            Response response = servicesSupport.executeRequest(spec,"PUT", endpoint_pet);
            Serenity.setSessionVariable("response").to(response);
        }
        catch(Exception e){
            e.getMessage();
        }
    }

    /**
     * Performs a PUT operation with an ID from the scenario as a parameter
     * @param file The JSON file of a user
     * @param username The UserName of a user
     * */
    @Step
    public void updateUserById(String file, String username) {

        try {
            InputStream is = this.getClass().getResourceAsStream("/requests/"+file);
            JSONObject body = servicesSupport.jsonInputStreamToJsonObject(is);
            spec = spec.body(body.toMap());
            String endpoint = endpoint_user + "/" + username;
            Response response = servicesSupport.executeRequest(spec,"PUT", endpoint);
            Serenity.setSessionVariable("response").to(response);
        }
        catch(Exception e){
            e.getMessage();
        }
    }

    /**
     * Performs a GET operation with two strings from the scenario as parameters
     * @param username The UserName of a user
     * @param password The Password of a user
     * */
    @Step
    public void loginUser(String username, String password) {

        String endpoint = endpoint_user + "/login?username=" + username + "&password="+ password;
        Response response = servicesSupport.executeRequest(spec,"GET", endpoint);
        Serenity.setSessionVariable("response").to(response);
    }

    /**
     * Performs a GET operation with no parameters
     * */
    @Step
    public void logoutUser() {

        String endpoint = endpoint_user + "/logout";
        Response response = servicesSupport.executeRequest(spec,"GET", endpoint);
        Serenity.setSessionVariable("response").to(response);
    }

    /**
     * Performs a DELETE operation with an ID provided by parameter from the scenario
     * @param id The ID of a pet
     * */
    @Step
    public void deletePetById(String id) {

        String endpoint = endpoint_pet + "/" + id;
        Response response = servicesSupport.executeRequest(spec,"DELETE", endpoint);
        Serenity.setSessionVariable("response").to(response);
    }

    /**
     * Performs a DELETE operation with an ID provided by parameter from the scenario
     * @param id The ID of an order
     * */
    @Step
    public void deleteOrderById(String id) {

        String endpoint = endpoint_order + "/" + id;
        Response response = servicesSupport.executeRequest(spec,"DELETE", endpoint);
        Serenity.setSessionVariable("response").to(response);
    }

    /**
     * Performs a DELETE operation with an ID provided by parameter from the scenario
     * @param username The username of a user
     * */
    @Step
    public void deleteUserById(String username) {

        String endpoint = endpoint_user + "/" + username;
        Response response = servicesSupport.executeRequest(spec,"DELETE", endpoint);
        Serenity.setSessionVariable("response").to(response);
    }


    /**
     * Performs a GET operation no parameters to check the inventory
     * */
    @Step
    public void getInventory() {
        Response response = servicesSupport.executeRequest(spec,"GET", endpoint_inventory);
        Serenity.setSessionVariable("response").to(response);
    }

    /**
     * Method to verify an status code received from the scenario
     * @param expectedStatusCode Expected status code in the response
     * */
    @Step
    public void verifyStatusCode(int expectedStatusCode){

        Response res = Serenity.sessionVariableCalled("response");
        Assert.assertEquals("status code doesn't match", expectedStatusCode, res.getStatusCode());
    }

    /**
     * Method to verify an status code received from the scenario
     * @param res Response object from a previous operation
     * @param operation The operation that was done in a previous step received from Cucumber
     * @param key Attribute name received from the scenario as a parameter
     * @param expectedValue Expected value of the attribute received from the scenario as a parameter
     * */
    @Step
    public void verifyValueFromKey(Response res, String operation, String key, String expectedValue) {

        String currentValue = "";

        switch (operation.toLowerCase()) {
            case "get":
                currentValue = res.getBody().jsonPath().getString(key);
                break;
            case "post":
                currentValue = res.getBody().jsonPath().getString(key);
                break;
            case "update":
                currentValue = res.getBody().jsonPath().getString(key);
                break;
            case "delete":
                currentValue = res.getBody().jsonPath().getString("data." + key);
                break;
            default:
                break;
        }

        Assert.assertEquals("Value for " + key + " doesn't match", expectedValue, currentValue);
    }
}