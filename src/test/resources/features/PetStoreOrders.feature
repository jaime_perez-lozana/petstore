Feature: Different services and scenarios with "store"

#  @influxdb
  Scenario Outline: Creating an order by ID and checking that the pet included has a certain ID
    When I request to create an order
    And the following json info is sent
      """
      {
        "id": 10,
        "petId": 23,
        "quantity": 10,
        "shipDate": "2018-08-23T10:40:56.013Z",
        "status": "placed",
        "complete": false
      }
      """
    Then I request to get an order by ID "<orderID>"
    And I should get <statusCode> status code
    And The value for the "<key>" after get operation should be "<value>"

    Examples:
      | orderID | key   | value | statusCode |
      | 10      | petId | 23    | 200        |

  Scenario Outline: Deleting an order with DELETE method and checking that the deletion was correctly done
    When I request to delete an order by ID "<orderID>"
    Then I request to get an order by ID "<orderID>"
    And I should get <statusCode> status code
    Examples:
      | orderID | statusCode |
      | 10      | 404        |

  Scenario Outline: Checking the inventory and see if a certain parameter is included
    When I request to check the inventory
    Then I should get <statusCode> status code
    #And The value for the "<key>" after get operation should be "<value>"
    Examples:
      | statusCode |
      | 200        |