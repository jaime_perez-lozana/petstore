Feature: Different services and scenarios with "store"

#  @influxdb
  Scenario Outline: Creating a user by ID and checking that the user included has a certain username
    And I request to create a user with this JSON "<jsonFile>"
    Then I request to get a user by ID "<value>"
    And I should get <statusCode> status code
    And The value for the "<key>" after get operation should be "<value>"
    And I request to delete a user by ID "<value>"
    #I delete the users because I don't want to interfere with the next scenario.

    Examples:
      | jsonFile            | key      | value      | statusCode |
      | create_user_11.json | username | user_alt   | 200        |
      | create_user_99.json | username | user_sugus | 200        |

  Scenario Outline: Creating a user with array by ID and checking that the user included has a certain username
    And I request to create a user by array with this JSON "<jsonFile>"
    Then I request to get a user by ID "<value>"
    And I should get <statusCode> status code
    And The value for the "<key>" after get operation should be "<value>"

    Examples:
      | jsonFile                  | key      | value      | statusCode |
      | create_user_array_11.json | username | user_alt   | 200        |
      | create_user_array_99.json | username | user_sugus | 200        |

  Scenario Outline: Updating a user with PUT method and checking that the update was correctly done
    When I request to update a user by this JSON "<jsonFile>" and username "<beforeUsername>"
    Then I request to get a user by ID "<afterUsername>"
    And I should get <statusCode> status code
    And The value for the "<key>" after get operation should be "<afterUsername>"
    Examples:
      | beforeUsername | key      | afterUsername | statusCode | jsonFile            |
      | user_alt       | username | user_ctrl     | 200        | update_user_11.json |
      | user_sugus     | username | user_agr      | 200        | update_user_99.json |

  Scenario Outline: Login of a user with his username and password
    When I request to login with a user by username "<username>" and password "<password>"
    And I should get <statusCode> status code
    Then I request to logout the user
    Examples:
      | username  | password  | statusCode |
      | user_ctrl | ctrl_user | 200        |
      | user_agr  | agr_user  | 200        |

  Scenario Outline: Deleting a user with DELETE method and checking that the delete was correctly done
    When I request to delete a user by ID "<username>"
    Then I request to get a user by ID "<username>"
    And I should get <statusCode> status code
    Examples:
      | username  | statusCode |
      | user_ctrl | 404        |
      | user_agr  | 404        |