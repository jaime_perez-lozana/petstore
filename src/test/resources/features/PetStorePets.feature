Feature: Different services and scenarios with "pets"

#  @influxdb
#  Scenario Outline: Get an existing pet by ID and checking that some key has a certain value
#    When I request to get a pet by ID "<petID>"
#    Then I should get <expectedStatusCode> status code
#    And The value for the "<key>" after get operation should be "<value>"
#
#    Examples:
#      | petID | key  | value                    | expectedStatusCode |
#      | 2     | name | testpet                  | 200                |
#      | 3     | name | 273836%%!(*8!(!*^!*&!&^8 | 200                |
#      | 4     | name | PatricksDog              | 200                |

  Scenario Outline: Adding a pet with POST method
    When I request to create a pet with this JSON "<jsonFile>"
#    Then I request to get a pet by ID "<petID>"
    And I should get <statusCode> status code
#    And The value for the "<key>" after get operation should be "<value>"
    Examples:
      | petID | key  | value | statusCode | jsonFile           |
      | 23    | name | sugus | 200        | create_pet_23.json |
      | 66    | name | rex   | 200        | create_pet_66.json |

  Scenario Outline: Get an existing pet by ID
    When I request to get a pet by ID "<petID>"
    Then I should get <expectedStatusCode> status code
    Examples:
      | petID | expectedStatusCode |
      | 23    | 200                |
      | 0     | 404                |
      | 66    | 200                |

  Scenario Outline: Updating a pet with PUT method
    When I request to update a pet by ID "<petID>"
    Then I request to get a pet by ID "<petID>"
    And I should get <statusCode> status code
    And The value for the "<key>" after get operation should be "<value>"
    Examples:
      | petID | key  | value | statusCode |
      | 23    | name | doge  | 200        |
      | 66    | name | max   | 200        |

  Scenario Outline: Deleting a pet with DELETE method
    When I request to delete a pet by ID "<petID>"
    Then I request to get a pet by ID "<petID>"
    And I should get <statusCode> status code
    Examples:
      | petID | statusCode |
      | 23    | 404        |
      | 66    | 404        |